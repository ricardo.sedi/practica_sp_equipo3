---- CREACIÓN DE SCHEMA ------

CREATE SCHEMA equipo3;

-- CREACIÓN DE TABLAS  ----

CREATE TABLE equipo3.zonas(
	idz NUMERIC(3,0),
	nombre VARCHAR(60),
	clasificacion_zona VARCHAR(3),
 	CONSTRAINT pk_zonas PRIMARY KEY (idz),
	CONSTRAINT ck_zonas_clasificacion CHECK (clasificacion_zona IN ('AAA','AA','A'))
);

CREATE TABLE equipo3.clientes(
	idc NUMERIC(3,0),
	idz NUMERIC(3,0),
	nombre VARCHAR(100),
 	CONSTRAINT pk_clientes PRIMARY KEY (idc),
	CONSTRAINT fk_clientes_zonas FOREIGN KEY (idz) REFERENCES equipo3.zonas(idz) 
		ON DELETE RESTRICT 
		ON UPDATE CASCADE
);

CREATE TABLE equipo3.vendedores(
	idv NUMERIC(3,0),
	idz NUMERIC(3,0),
	nombre VARCHAR(30),
	apellido VARCHAR(30),
	CONSTRAINT pk_vendedores PRIMARY KEY (idv),
	CONSTRAINT fk_vendedores_zonas  
	FOREIGN KEY (idz) REFERENCES equipo3.zonas(idz) 
		ON DELETE RESTRICT 
		ON UPDATE CASCADE
);

CREATE TABLE equipo3.ventas(
	no_ticket SERIAL,
	idv NUMERIC(3,0),
	idc NUMERIC(3,0),
	fecha DATE,
	importe NUMERIC(9,2),
	iva NUMERIC(9,2),
	venta_total NUMERIC(9,2),
    CONSTRAINT pk_ventas PRIMARY KEY (no_ticket),
	CONSTRAINT fk_ventas_vendedores  FOREIGN KEY (idv) REFERENCES equipo3.vendedores(idv) 
		ON DELETE RESTRICT 
		ON UPDATE RESTRICT,
	CONSTRAINT fk_ventas_clientes  FOREIGN KEY (idc) REFERENCES equipo3.clientes(idc) 
		ON DELETE RESTRICT 
		ON UPDATE RESTRICT
);

CREATE TABLE equipo3.comisiones(
	idv NUMERIC(3,0),
	anio INTEGER,
	mes INTEGER,
	venta_total NUMERIC(9,2),
	comision NUMERIC(9,2),
	invadio_zona CHAR(1),
	CONSTRAINT pk_comisiones PRIMARY KEY (idv, anio, mes),
	CONSTRAINT fk_comisiones FOREIGN KEY (idv) REFERENCES equipo3.vendedores(idv)
		ON DELETE RESTRICT 
		ON UPDATE CASCADE, 
	CONSTRAINT ck_comisiones_invadio_zona CHECK (invadio_zona IN ('S','N'))
);

CREATE TABLE equipo3.auditoria(
	anio INTEGER,
	mes INTEGER,
	id_vendedor NUMERIC(3,0),
	id_zona NUMERIC(3,0),
	clasificacion_zona VARCHAR(3),
	por_comision NUMERIC(9,2),
	venta_total_mensual NUMERIC(9,2),
	comision NUMERIC(9,2),
	penalizacion NUMERIC(9,2),
	comision_total NUMERIC(9,2)
);

--INSERTS PARA PRUEBAS--
INSERT INTO equipo3.zonas VALUES
(1, 'CDMX SUR', 'A'),
(2, 'CDMX NORTE', 'A'),
(3, 'CDMX CENTRO', 'A'),
(4, 'CDMX PONIENTE', 'A'),
(5, 'EDOMEX NORTE', 'A'),
(6, 'EDOMEX ORIENTE', 'A'),
(7, 'EDOMEX SUR', 'A'),
(8, 'HIDALGO', 'A'),
(9, 'PUEBLA', 'A'),
(10, 'QUERETARO', 'A'),
(999, 'Desconocido', 'A');

INSERT INTO equipo3.vendedores VALUES
(1, 1, 'Rafael', 'Soto'), 
(2, 1, 'Ricardo', 'Arzate'), 
(3, 2, 'Raymundo', 'Urbina'), 
(4, 2, 'Viridiana', 'Hernandez'), 
(5, 3, 'Martin ', 'Retana'), 
(6, 3, 'Mario', 'Garcia'), 
(7, 4, 'Elizabeth', 'Garcia'), 
(8, 4, 'Consuelo', 'Aviles'), 
(9, 5, 'Elias', 'Ukan'), 
(10, 6, 'Aceneth', 'Hernandez'), 
(11, 7, 'Salvador', 'Avila'), 
(12, 8, 'Armando', 'Martinez'), 
(13, 9, 'Alicia', 'Cordoba'), 
(14, 10, 'Ana', 'Macias'),
(999, 999,  'Desconocido', 'Desconocido');

INSERT INTO equipo3.clientes VALUES
(10, 1, 'La sazon'), 
(20, 1, 'Cantina la Jalisiense'), 
(30, 1, 'Wings Motors'), 
(40, 1, 'Bar Habos'), 
(50, 1, 'El ogro'), 
(60, 2, 'Tecla de sol'), 
(70, 2, 'Doghouse'), 
(80, 2, 'La clandestina'), 
(90, 3, 'La Alhambra '), 
(100, 3, 'Cantabria'), 
(110, 3, 'Resturante Manolo'), 
(120, 3, 'La vitola'), 
(130, 3, 'Rincon de Luis'), 
(140, 3, 'La sureña'), 
(150, 3, 'La mula de Moscu'), 
(160, 3, 'El perro Andaluz'), 
(170, 3, 'Chinaloa'), 
(180, 3, 'Resturante Montejo'), 
(190, 4, 'Un lugar de la mancha'), 
(200, 4, 'Gin Gin'), 
(210, 4, 'Beer Station'), 
(220, 7, 'Torito Sinaloense'), 
(230, 5, 'El puerto'), 
(240, 6, 'La perla'), 
(250, 9, 'TEODIO'), 
(260, 10, 'Bar Veider'), 
(270, 10, 'La bodeguita'), 
(280, 10, 'Paradiso Perduto'), 
(290, 8, 'El asadero'), 
(300, 9, 'La terraza'); 

INSERT INTO equipo3.ventas(idv, idc, fecha, importe, iva, venta_total) VALUES
(1, 40, '03/02/2020', 12000, 1920, 13920), 
(1, 30, '10/03/2020', 15000, 2400, 17400), 
(1, 10, '20/02/2020', 8000, 1280, 9280), 
(1, 90, '25/02/2020', 13000, 2080, 15080), 
(1, 150, '05/03/2020', 10000, 1600, 11600), 
(2, 20, '06/02/2020', 7000, 1120, 8120), 
(2, 50, '18/02/2020', 6000, 960, 6960), 
(2, 110, '22/02/2020', 4000, 640, 4640), 
(3, 80, '28/02/2020', 10000, 1600, 11600), 
(3, 70, '04/03/2020', 15000, 2400, 17400), 
(3, 130, '06/03/2020', 22000, 3520, 25520), 
(3, 180, '12/03/2020', 18000, 2880, 20880), 
(4, 60, '20/01/2020', 4000, 640, 4640), 
(4, 140, '10/01/2020', 6000, 960, 6960), 
(5, 90, '18/02/2020', 9000, 1440, 10440), 
(5, 100, '11/01/2020', 9000, 1440, 10440), 
(5, 100, '08/02/2020', 14000, 2240, 16240), 
(5, 110, '15/02/2020', 18000, 2880, 20880), 
(6, 160, '30/01/2020', 20000, 3200, 23200), 
(6, 170, '06/02/2020', 17000, 2720, 19720), 
(6, 180, '28/01/2020', 15000, 2400, 17400), 
(6, 90, '22/01/2020', 12000, 1920, 13920), 
(7, 130, '15/03/2020', 13000, 2080, 15080), 
(7, 100, '23/01/2020', 8000, 1280, 9280), 
(8, 200, '14/02/2020', 9000, 1440, 10440), 
(8, 190, '17/02/2020', 9000, 1440, 10440), 
(9, 210, '10/03/2020', 7000, 1120, 8120), 
(10, 70, '11/01/2020', 2000, 320, 2320), 
(11, 220, '27/02/2020', 19000, 3040, 22040), 
(12, 290, '11/02/2020', 20000, 3200, 23200), 
(13, 250, '05/03/2020', 17000, 2720, 19720), 
(14, 260, '10/01/2020', 14000, 2240, 16240), 
(14, 260, '03/02/2020', 4000, 640, 4640), 
(14, 270, '27/02/2020', 2000, 320, 2320), 
(14, 280, '10/02/2020', 25000, 4000, 29000);

INSERT INTO equipo3.comisiones VALUES
(1, 2020, 2, 38280, 3445.2, 'N'), 
(1, 2020, 3, 29000, 2610, 'N'), 
(2, 2020, 2, 19720, 1774.8, 'N'), 
(3, 2020, 2, 11600, 1044, 'N'), 
(3, 2020, 3, 63800, 5742, 'N'), 
(4, 2020, 1, 11600, 1044, 'N'), 
(5, 2020, 1, 10440, 939.6, 'N'), 
(5, 2020, 2, 47560, 4280.4, 'N'), 
(6, 2020, 1, 54520, 4906.8, 'N'), 
(6, 2020, 2, 19720, 1774.8, 'N'), 
(7, 2020, 1, 9280, 835.2, 'N'), 
(7, 2020, 3, 15080, 1357.2, 'N'), 
(8, 2020, 2, 20880, 1879.2, 'N'), 
(9, 2020, 3, 8120, 730.8, 'N'), 
(10, 2020,  1, 2320, 208.8, 'N'), 
(11, 2020,  2, 22040, 1983.6, 'N'), 
(12, 2020,  2, 23200, 2088, 'N'), 
(13, 2020,  3, 19720, 1774.8, 'N'), 
(14, 2020,  1, 16240, 1461.6, 'N'), 
(14, 2020,  2, 35960, 3236.4, 'N');

CREATE OR REPLACE FUNCTION equipo3.sp_3 (NUMERIC(3,0), NUMERIC(3,0),VARCHAR(30), VARCHAR(30))
                                                                                           
RETURNS VARCHAR
AS $sp_3$

DECLARE
  vidv ALIAS FOR $1;
  vidz ALIAS FOR $2;
  vnombre ALIAS FOR $3;
  vapellido ALIAS FOR $4;
  resultado VARCHAR;
  
BEGIN
  IF EXISTS (SELECT * FROM equipo3.vendedores WHERE nombre = vnombre AND apellido = vapellido) THEN

    resultado:='No puede haber vendedores repetidos';

  ELSE
   
    UPDATE equipo3.vendedores 
    SET idz=vidz, nombre=vnombre, apellido=vapellido 
    WHERE idv=vidv;

    IF FOUND THEN
  
      resultado:='Vendedor actualizado';

    ELSE

      INSERT INTO equipo3.vendedores VALUES(vidv,vidz, vnombre,vapellido);
	
      resultado:='Nuevo vendedor registrado';

    END IF;
  END IF;

RETURN resultado;

END;
$sp_3$
LANGUAGE plpgsql;

--SP MONICA Y DAVID--

CREATE OR REPLACE FUNCTION equipo3.sp_4 (NUMERIC (3,0))
returns VARCHAR
AS $sp_4$
DECLARE
	vidc ALIAS for $1;
	vidz NUMERIC (3,0);
	vnombrez VARCHAR(60);
	vnombreC VARCHAR(100);
	resultado VARCHAR(100);
BEGIN
	SELECT INTO vnombreC nombre FROM equipo3.clientes WHERE idc=vidc;
	SELECT INTO vidz idz FROM equipo3.clientes WHERE idc=vidc;
	SELECT INTO vnombrez nombre FROM equipo3.zonas WHERE idz=vidz;
	resultado = CONCAT('Nombre Cliente: ' , vnombreC, CHR(10), 'Nombre Zona: ', vnombrez);
	RETURN resultado;
END;
$sp_4$
LANGUAGE plpgsql;

--CALCULA EL IVA Y LA VENTA TOTAL, TAMBIEN CAMBIA UNA BANDERA SI EL VENDEDOR INVADIO UNA ZONA--
CREATE OR REPLACE FUNCTION equipo3.sp_cada_venta()
RETURNS TRIGGER
AS $sp_cada_venta$
	DECLARE
	id_zona_cliente NUMERIC(3,0);
	id_zona_vendedor NUMERIC(3,0);
	id_vendedor INTEGER;
	ventas_venta_total NUMERIC(9,2);
	comisiones_venta_total NUMERIC(9,2);
	nuevo_comisiones_venta_total NUMERIC(9,2);
	mes_venta INTEGER;
	anio_venta INTEGER;
	iva_calculado NUMERIC (9,2);
	vt_calculado NUMERIC (9,2);
	BEGIN
		IF (TG_OP = 'INSERT') THEN
			--CALCULAR IVA Y VENTA TOTAL--
			iva_calculado = NEW.importe * .16;
			vt_calculado = (NEW.importe + iva_calculado);
			UPDATE equipo3.ventas
				SET 
				iva = iva_calculado,
				venta_total = vt_calculado
			WHERE no_ticket=NEW.no_ticket;
			--SI AUN NO EXISTE REGISTRO DE COMISIONES DE ESE AÑO Y MES CREARLO, SI EXISTE ACTUALIZARLO SUMANDO LA VENTA ACTUAL A SU VENTA TOTAL DEL VENDEDOR--
			mes_venta = EXTRACT(MONTH FROM (NEW.fecha));
			anio_venta = EXTRACT(YEAR FROM (NEW.fecha));
			SELECT INTO comisiones_venta_total venta_total FROM equipo3.comisiones WHERE idv=NEW.idv AND mes=mes_venta AND anio=anio_venta;					  
			IF NOT FOUND THEN
				INSERT INTO equipo3.comisiones VALUES(NEW.idv, anio_venta, mes_venta, vt_calculado, 0, 'N');
			ELSE	
				nuevo_comisiones_venta_total = vt_calculado + comisiones_venta_total;
				UPDATE equipo3.comisiones
					SET venta_total=nuevo_comisiones_venta_total
					WHERE idv=NEW.idv AND mes=mes_venta AND anio=anio_venta;
			END IF;
			--VERIFICAR SI NO INVADIO UNA ZONA--
			SELECT INTO id_zona_cliente idz FROM equipo3.clientes WHERE idc=NEW.idc;
			SELECT INTO id_zona_vendedor idz FROM equipo3.vendedores WHERE idv=NEW.idv;
			IF id_zona_cliente != id_zona_vendedor THEN
				IF NEW.fecha > '2020-02-14' THEN
					UPDATE equipo3.comisiones
						SET invadio_zona='S'
						WHERE idv=NEW.idv AND mes=mes_venta AND anio=anio_venta;
				END IF;
			END IF;
			--RECALCULAR CLASIFICACIONES--
			PERFORM equipo3.sp_recalcular_clasificacion_zona(NEW.fecha);
			PERFORM equipo3.sp_calcular_comisiones(NEW.fecha, NEW.idv);
			RETURN NEW;
		--NO ESTAN PERMITIDAS LAS ACTUALIZACIONES--
		ELSIF (TG_OP = 'UPDATE') THEN
			RAISE EXCEPTION 'No se permite modificiones a las ventas, en caso de error elimine la venta y registrela otra vez con la informacion correcta, gracias.';
		--AL BORRAR UNA VENTA, SE DESCUENTA DE LA VENTA TOTAL DEL VENDEDOR EN ESE MES Y AÑO REQUIERE TICKET--
		ELSIF (TG_OP = 'DELETE') THEN
			id_vendedor= OLD.idv;
			mes_venta = EXTRACT(MONTH FROM (OLD.fecha));
			anio_venta = EXTRACT(YEAR FROM (OLD.fecha));
			SELECT INTO comisiones_venta_total venta_total FROM equipo3.comisiones WHERE idv=id_vendedor AND mes=mes_venta AND anio=anio_venta;
			nuevo_comisiones_venta_total = comisiones_venta_total - OLD.venta_total;
			UPDATE equipo3.comisiones
				SET venta_total=nuevo_comisiones_venta_total
				WHERE idv=id_vendedor AND mes=mes_venta AND anio=anio_venta;
			--RECALCULAR CLASIFICACIONES--	
			PERFORM equipo3.sp_recalcular_clasificacion_zona(OLD.fecha);
			PERFORM equipo3.sp_calcular_comisiones(OLD.fecha, 0);
			RETURN OLD;
		END IF;
	END;
$sp_cada_venta$
LANGUAGE plpgsql;

--RECALCULAR CLASIFICACION DE LAS ZONAS--
CREATE OR REPLACE FUNCTION equipo3.sp_recalcular_clasificacion_zona(DATE)
RETURNS VOID
AS $sp_recalcular_clasificacion_zona$
    DECLARE
	fecha_venta ALIAS FOR $1;
    promedio_zona_mes NUMERIC(9,2);
    suma_todas_zona_mes NUMERIC(9,2);
    suma_zona_mes NUMERIC(9,2);
    numero_zonas INTEGER;
    clasificacion VARCHAR;
    fecha_inicio DATE;
    fecha_fin DATE;
    t_row equipo3.zonas%rowtype;
 
    BEGIN
       
       fecha_inicio = DATE_TRUNC('MONTH', fecha_venta);
	   fecha_fin = DATE_TRUNC('MONTH', fecha_venta) + INTERVAL '1 MONTH';

	   /*Obtenemos el promedio de ventas por mes. Primero hacemos la suma de todas las ventas del mes y lo dividimos entre las zonas existentes*/
	   SELECT INTO suma_todas_zona_mes SUM(venta_total) FROM equipo3.ventas WHERE fecha >= fecha_inicio AND fecha < fecha_fin;
	   SELECT INTO numero_zonas COUNT(*) FROM equipo3.zonas;
	   promedio_zona_mes = suma_todas_zona_mes/numero_zonas;

	   FOR t_row in SELECT * FROM equipo3.zonas LOOP
		   SELECT INTO suma_zona_mes SUM(v.venta_total) FROM equipo3.ventas AS v NATURAL JOIN equipo3.clientes AS c WHERE c.idz = t_row.idz AND fecha >= fecha_inicio AND fecha < fecha_fin;
		   CASE
			   WHEN (suma_zona_mes > (promedio_zona_mes + 20000)) THEN
				   clasificacion = 'AAA';
			   WHEN (suma_zona_mes <= (promedio_zona_mes - 5000)) THEN
				   clasificacion = 'A';
			   ELSE
				   clasificacion = 'AA';
		   END CASE;
		   UPDATE equipo3.zonas
		   SET clasificacion_zona = clasificacion
		   WHERE idz = t_row.idz;
	   END LOOP;      
END;
$sp_recalcular_clasificacion_zona$
LANGUAGE plpgsql;

--CALCULAR LAS COMISIONES--
CREATE OR REPLACE FUNCTION equipo3.sp_calcular_comisiones(DATE, NUMERIC)
RETURNS VOID
AS $sp_calcular_comisiones$
    DECLARE
	
	fecha_venta ALIAS FOR $1;
	id_vendedor ALIAS FOR $2;
	id_zona NUMERIC;
    anio_venta NUMERIC;
	mes_venta NUMERIC;
	fecha_inicio DATE;
    fecha_fin DATE;
    clasificacion_vendedor VARCHAR;
	venta_total_mensual NUMERIC;
	porcentaje_comision NUMERIC;
	comision_neta NUMERIC;
	penalizacion NUMERIC;
	comision_final NUMERIC;
	invasor CHAR;
    t_row equipo3.comisiones%rowtype;
	g_row equipo3.ventas%rowtype;
 
    BEGIN
		fecha_inicio = DATE_TRUNC('MONTH', fecha_venta);
	   	fecha_fin = DATE_TRUNC('MONTH', fecha_venta) + INTERVAL '1 MONTH';
		mes_venta = EXTRACT(MONTH FROM (fecha_venta));
		anio_venta = EXTRACT(YEAR FROM (fecha_venta));	
		
		FOR t_row in SELECT * FROM equipo3.comisiones WHERE mes=mes_venta AND anio=anio_venta LOOP
			comision_neta = 0;
			FOR g_row in SELECT * FROM equipo3.ventas WHERE idv=t_row.idv AND fecha >= fecha_inicio AND fecha < fecha_fin LOOP
				IF g_row.fecha < '2020-02-15' THEN
					porcentaje_comision = 0.09;
				ELSE
					SELECT INTO clasificacion_vendedor clasificacion_zona FROM equipo3.vendedores AS v JOIN equipo3.zonas AS z ON z.idz=v.idz WHERE v.idv = t_row.idv;
					CASE clasificacion_vendedor
					   WHEN 'AAA' THEN
						   porcentaje_comision = 0.08;
					   WHEN 'AA'THEN
						   porcentaje_comision = 0.10;
					   ELSE
						   porcentaje_comision = 0.12;
					END CASE;
				END IF;
				comision_neta = comision_neta + (g_row.venta_total * porcentaje_comision);
			END LOOP;
			SELECT INTO invasor invadio_zona FROM equipo3.comisiones WHERE idv= t_row.idv AND mes=mes_venta AND anio=anio_venta;
			IF (invasor = 'S') THEN
				penalizacion = comision_neta * .02;
			ELSE
				penalizacion=0;
			END IF;
			comision_final = comision_neta - penalizacion;
			  
			IF id_vendedor = t_row.idv THEN
				SELECT INTO id_zona idz FROM equipo3.vendedores WHERE idv=id_vendedor;
				SELECT INTO venta_total_mensual venta_total FROM equipo3.comisiones WHERE idv= t_row.idv AND mes=mes_venta AND anio=anio_venta;
				INSERT INTO equipo3.auditoria VALUES (anio_venta, mes_venta, id_vendedor,  id_zona, clasificacion_vendedor, porcentaje_comision, venta_total_mensual, comision_neta, penalizacion, comision_final);
			END IF;			
			UPDATE equipo3.comisiones
				SET comision = comision_final
				WHERE idv = t_row.idv AND mes=mes_venta AND anio=anio_venta;
	   END LOOP;	
END;
$sp_calcular_comisiones$
LANGUAGE plpgsql;

--CADA QUE SE REALIZA UNA VENTA SE EJECUTA EL SIGUIENTE PROCESO--
CREATE TRIGGER tg_cada_venta
AFTER INSERT OR UPDATE OR DELETE ON equipo3.ventas
FOR EACH ROW
WHEN (pg_trigger_depth()=0)
EXECUTE PROCEDURE equipo3.sp_cada_venta();