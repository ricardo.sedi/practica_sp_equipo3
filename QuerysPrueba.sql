--SELECTS--

SELECT * FROM equipo3.comisiones WHERE idv=8 AND mes=2
SELECT * FROM equipo3.ventas WHERE fecha BETWEEN '2020-02-01' AND '2020-02-29' ORDER BY fecha
SELECT * FROM equipo3.vendedores
SELECT * FROM equipo3.clientes
SELECT * FROM equipo3.zonas
SELECT * FROM equipo3.auditoria
SELECT * FROM equipo3.ventas WHERE fecha BETWEEN '2020-02-01' AND '2020-02-29' AND idv=8 ORDER BY fecha

--DIFERENCIA ENTRE LAS VENTAS TOTALES DE UNA ZONA Y EL PROMEDIO DE VENTAS, EN UN MES Y AÑO ESPECIFICO--
SELECT
	--VENTAS DE UNA ZONA EN UN MES Y AÑO ESPECIFICOS--
	(SELECT SUM(v.venta_total) FROM equipo3.ventas AS v NATURAL JOIN equipo3.clientes AS c WHERE c.idz=7 AND fecha >= '2020-03-01' AND fecha < '2020-04-01')-
	--PROMEDIO DE VENTAS EN UN MES Y AÑO ESPECIFICOS--
	(SELECT (SELECT SUM(venta_total) FROM equipo3.ventas WHERE fecha >= '2020-03-01' AND fecha < '2020-04-01')/(SELECT COUNT(*) FROM equipo3.zonas));

--INSERT UPDATES DELETE--

INSERT INTO equipo3.ventas(idv, idc, fecha, importe) VALUES
(1, 200, '2020-03-04', 20000);

UPDATE equipo3.ventas
SET importe=9000000
WHERE no_ticket=50;

DELETE FROM equipo3.ventas WHERE no_ticket=47;
